require('nodefly').profile(
	'35ac1becc81ab48e9f39e4bfdb92b0aa',
	'sweario'
);

var http = require('http'),
	Db = require('mongodb').Db,
	Server = require('mongodb').Server,
	Connection = require('mongodb').Connection;

var port = process.env.PORT || 3000;

var mongohost = process.env['MONGO_NODE_DRIVER_HOST'] != null ? process.env['MONGO_NODE_DRIVER_HOST'] : 'localhost',
	mongoport = process.env['MONGO_NODE_DRIVER_PORT'] != null ? process.env['MONGO_NODE_DRIVER_PORT'] : Connection.DEFAULT_PORT,
	mongouser = process.env['MONGO_NODE_DRIVER_USER'] != null ? process.env['MONGO_NODE_DRIVER_USER'] : '',
	mongopass = process.env['MONGO_NODE_DRIVER_PASS'] != null ? process.env['MONGO_NODE_DRIVER_PASS'] : '',
	db = new Db('sweario', new Server(mongohost, mongoport), {w: 1, native_parser: false});

var decorator = require('./decorator'),
	routes = require('./routes'),
	router = routes(db);

var server = http.createServer(function( req, res ){
	decorator(req, res);

	router.dispatch( req, res );
});

db.open(function(err, db) {
  if(err) throw err
  db.authenticate(mongouser, mongopass, function(err, result) {
  	server.listen(port);
  })
});
