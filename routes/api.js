var wordsModel = require('../models/words');

module.exports = function(router, db) {
	var words = wordsModel(db);

	router.get('api', function(req, res, params) {
		var response = {}
		response.welcomeMessage = "Welcome to Swear.io's API"
		res.json(response);
	});

	router.get('api/list', function(req, res, params) {
		res.json(words.all());
	});

	router.get('api/random', function(req, res, params) {
		var response = {};
		var num = 1;

		if (params !== undefined && params.n !== undefined) {
			num = params.n;
		}

		words.random(num, function(err, wordList) {
			response.wordList = wordList;
			res.json(response);
		});
	});
}
