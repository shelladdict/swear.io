module.exports = function( router, db ){
  var words = require('../models/words')(db);
  // Serve all static files in the `assets` directory with ecstatic
  router.get('', function(req, res, params) {
  	words.random(1, function(err, wordList) {
			res.template('index.ejs', { "word": wordList[0]});
		});

  });

};
