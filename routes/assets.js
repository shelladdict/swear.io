var ecstatic = require('ecstatic');
var path = require('path');

var ec = ecstatic( path.resolve(__dirname + '/..' ) );

module.exports = function( router, client ){

  // Serve all static files in the `assets` directory with ecstatic
  router.add('assets/*path', ec);
};
