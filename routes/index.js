var ramrod = require('ramrod');

var router = ramrod();

//
// Routes Interface - attaches all route events from routes submodules
//
// Pass in Global objects / Connections here
//
//  returns a Ramrod Router instance
module.exports = function(db) {
	var assets = require('./assets');
	assets(router);

	var api = require('./api');
	api(router, db);

	var site = require('./site');
	site(router, db);

	// Serve a 404 for any unmatched routes
	router.on('*', function(req, res) {
		res.error(404);
	});

	return router;
}
