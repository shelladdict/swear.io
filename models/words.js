var fs = require('fs');

module.exports = function(db) {
	var model = {};
	var wordList = JSON.parse( require( 'fs' ).readFileSync( __dirname + '/../word_list.json' ) );

	model.all = function () {
		db.collection('words', function(err, collection) {
			// Fetch all docs for rendering of list
			collection.find({}).toArray(function(err, items) {
				if (err) {
					console.log("Error: ", err);
					return err;
				}
				return items;
			});
		});
	}

	model.random = function (num, callback) {
		randomSearch = Math.random();
		db.collection('words', function(err, collection) {
			collection.find({random: {$gte:randomSearch}}).sort({random: 1}).limit(num).toArray(function(err, item){
				if (err) {
					console.log("Error: ", err);
					callback(err);
				} else if (item == null) {
					collection.find({random: {$lte:randomSearch}}).sort({random: -1}).limit(num).toArray(function(err2, item2){
						if (err) {
							console.log("Error: ", err);
							callback(err);
						}
						var response = []
						item2.forEach(function(item){
							response.push(item.word);
						})
						callback(null, response);
					})
				} else {
						var response = []
						item.forEach(function(item){
							response.push(item.word);
						})
						callback(null, response);
				}
			});
		});
	}

	return model;
}
